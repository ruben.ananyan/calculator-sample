﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class CalculatorForm : Form
    {

        private double labelValue = 0;
        private string operation = "";
        private bool isPressed = false;


        public CalculatorForm()
        {
            InitializeComponent();
            SetStyle(ControlStyles.Selectable, false);

        }

        private void OnDigitClick(object sender, EventArgs e)
        {
            if ((resultBox.Text == "0") || (isPressed))
            {
                resultBox.Clear();
            }

            isPressed = false;
            Button button = (Button)sender;

            resultBox.Text += button.Text;
        }

        private void OnDotClick(object sender, EventArgs e)
        {
            isPressed = false;
            Button button = (Button)sender;

            if (button.Text == ".")
            {
                if (!resultBox.Text.Contains("."))
                {
                    resultBox.Text += button.Text;
                }
            }
        }

        private void OnCeClick(object sender, EventArgs e)
        {
            resultBox.Text = "0";
        }

        private void OnOperatorClick(object sender, EventArgs e)
        {
            Button ariphmeticButton = (Button)sender;
            double resultBoxValue = "".Equals(resultBox.Text) ? 0.0 : double.Parse(resultBox.Text);

           
            isPressed = true;

            if (labelValue != 0)
            {
                equalsButton.PerformClick();
                operation = ariphmeticButton.Text;
                //equationLabel.Text = resultBoxValue + operation;
            }
            else
            {
                labelValue = resultBoxValue;
                operation = ariphmeticButton.Text;
            }
        }

        private void OnEqualsClick(object sender, EventArgs e)
        {
            double resultBoxValue = "".Equals(resultBox.Text) ? 0.0 : double.Parse(resultBox.Text);

            switch (operation)
            {
                case "+":
                    resultBox.Text = (labelValue + resultBoxValue).ToString();
                    labelValue += resultBoxValue;
                    break;
                case "-":
                    resultBox.Text = (labelValue - resultBoxValue).ToString();
                    labelValue -= resultBoxValue;
                    break;
                case "*":
                    resultBox.Text = (labelValue * resultBoxValue).ToString();
                    labelValue *= resultBoxValue;
                    break;
                case "/":
                    resultBox.Text = (labelValue / resultBoxValue).ToString();
                    labelValue /= resultBoxValue;
                    break;
                default:
                    break;
            }
            //labelValue = resultBoxValue;
            operation = "";
        }

        private void OnClearClick(object sender, EventArgs e)
        {
            resultBox.Text = "0";
            labelValue = 0;
        }

        private void calculatorForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar.ToString())
            {
                case "0":
                    zeroButton.PerformClick();
                    break;
                case "1":
                    oneButton.PerformClick();
                    break;
                case "2":
                    twoButton.PerformClick();
                    break;
                case "3":
                    threeButton.PerformClick();
                    break;
                case "4":
                    fourButton.PerformClick();
                    break;
                case "5":
                    fiveButton.PerformClick();
                    break;
                case "6":
                    sixButton.PerformClick();
                    break;
                case "7":
                    sevenButton.PerformClick();
                    break;
                case "8":
                    eightButton.PerformClick();
                    break;
                case "9":
                    nineButton.PerformClick();
                    break;
                case "+":
                    plusButton.PerformClick();
                    break;
                case "-":
                    minusButton.PerformClick();
                    break;
                case "*":
                    multiplicationButton.PerformClick();
                    break;
                case "/":
                    divisionButton.PerformClick();
                    break;
                case ".":
                    dotButton.PerformClick();
                    break;
                case "=":
                    equalsButton.PerformClick();
                    break;
                case "ENTER":
                    equalsButton.PerformClick();
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// This method was overridden because on Enter button key pressed the focused button was clicked, not onEqualsButtonClick event.
        /// So I couldn't overcome this situation using traditional methods.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                equalsButton.PerformClick();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
